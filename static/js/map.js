mapboxgl.accessToken = 'pk.eyJ1Ijoibm1vbmFyaXpxYSIsImEiOiJjaXhxYnduOGswNHJ2MndvNjJ0Yzh5ZTNuIn0.5qd_UgY904PzhnD6W9Qybg';
            
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/light-v9',
    center:  [-73.9980832,40.7302955],
    zoom: 13
});



map.on('load', function () {


    map.addLayer({
        "id": "lane",
        "type": "line",
        "source": {
            "type": "geojson",
            // "data": "{{ url_for('static', filename='data/bike_lane.geojson') }}"
            "data": "static/data/bike_lane.geojson"
            },
        "layout": {
            'visibility': 'visible'
        },
        "paint": {
            "line-color": "#4fb41d"
        }
    });


    
    map.addLayer({
        "id": "bikes",
        "type": "symbol",
        "source": {
            "type": "geojson",
            // "data": "{{ url_for('static', filename='data/citibike.geojson') }}"
            "data": "static/data/citibike.geojson"
            },
        'layout': {
            'visibility': 'visible',
            "text-field": [
                "step",
                ["zoom"],
                ["to-string", ["get", "num_bikes_available"]],
                22,
                ["to-string", ["get", "num_bikes_available"]]
            ],
            "text-size": [
                "interpolate",
                ["linear"],
                ["zoom"],
                0,
                5,
                22,
                14
            ],
            "icon-image": "bicycle-share-15",
            "text-anchor": "bottom-left",
            "text-offset": [0.5, -0.5],
            "text-font": ["Open Sans Bold", "Arial Unicode MS Regular"]
        },
        "paint": {
            "text-halo-color": [
                "interpolate",
                ["linear"],
                ["get", "num_bikes_available"],
                1,
                "hsl(0, 88%, 69%)",
                2,
                "hsl(196, 97%, 35%)",
                25,
                "hsl(196, 97%, 35%)",
                44,
                "hsl(196, 97%, 35%)"
            ],
            "text-halo-width": 10,
            "text-color": "hsl(0, 0%, 100%)"
        }
    });


    map.addLayer({
        "id": "docks",
        "type": "symbol",
        "source": {
            "type": "geojson",
            // "data": "{{ url_for('static', filename='data/citibike.geojson') }}"
            "data": "static/data/citibike.geojson"
            },

        "paint": {
            "text-translate": [0, 0],
            "text-color": "hsl(0, 0%, 100%)",
            "text-halo-width": 10,
            "text-halo-color": [
                "interpolate",
                ["linear"],
                ["get", "num_docks_available"],
                1,
                "hsl(0, 100%, 73%)",
                2,
                "hsl(196, 97%, 35%)",
                29,
                "hsl(196, 97%, 35%)",
                58,
                "hsl(196, 97%, 35%)"
            ]
        },
        'layout': {
            'visibility': 'visible',
            "text-field": ["to-string", ["get", "num_docks_available"]],
            "text-size": [
                "interpolate",
                ["linear"],
                ["zoom"],
                0,
                5,
                22,
                14
            ],
            "text-font": ["Open Sans Bold", "Arial Unicode MS Regular"],
            "text-anchor": "bottom-left",
            "text-offset": [0.5, -0.5],
            "icon-image": "building-alt1-15"
        }
        
    });

    map.addLayer({
        "id": "reports",
        "type": "circle",
        "source": {
            "type": "geojson",
            // "data": "{{ url_for('static', filename='data/mock_reports.geojson') }}"
            "data": "static/data/mock_reports.geojson"
            },
        "layout": {
            'visibility': 'visible'
        },
        "paint": {
            "circle-radius": [
                "interpolate",
                ["linear"],
                ["zoom"],
                0,
                5,
                22,
                10
            ],
            "circle-color": [
                "interpolate",
                ["linear"],
                ["get", "risk"],
                0,
                "#3EB003",
                1,
                "hsl(49, 95%, 53%)",
                2,
                "#ef2917"
            ],
            "circle-stroke-color": "hsl(0, 0%, 100%)",
            "circle-stroke-width": 1
        }
        
    });

    map.addLayer({
        "id": "current",
        "type": "circle",
        "source": {
            "type": "geojson",
            "data": {
                "type": "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [-73.9980832,40.7302955]
                    }
                }]
            }
        },
        "paint": {
            "circle-color": "#00A7E1",
            "circle-stroke-color": "hsl(0, 0%, 100%)",
            "circle-stroke-width": 3,
            "circle-radius": 5
        }
    });




});
var toggleableLayerIds = ['reports','lane'];

for (var i = 0; i < toggleableLayerIds.length; i++) {
    var id = toggleableLayerIds[i];

    var link = document.createElement('a');
    link.href = '#';
    link.className = 'active';
    link.textContent = id;

    link.onclick = function (e) {
        var clickedLayer = this.textContent;
        e.preventDefault();
        e.stopPropagation();

        var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

        if (visibility === 'visible') {
            map.setLayoutProperty(clickedLayer, 'visibility', 'none');
            this.className = '';
        } else {
            this.className = 'active';
            map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
        }
    };

    var layers = document.getElementById('menu');
    layers.appendChild(link);
}


map.on('click', 'reports', function (e) {
    var coordinates = e.features[0].geometry.coordinates.slice();
    var description = e.features[0].properties.hazard;
    var subtype = e.features[0].properties.subtypes;

    while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
    }

    new mapboxgl.Popup()
        .setLngLat(coordinates)
        .setHTML('<h3>'+description+'</h3><h4>'+subtype+'</h4>')
        .addTo(map);
    });

$("#citiSlider").on('change', function() {
    if ($(this).is(':checked')) {
        switchStatus = $(this).is(':checked');
        map.setLayoutProperty('bikes', 'visibility', 'none');
        map.setLayoutProperty('docks', 'visibility', 'visible');
    }
    else {
        switchStatus = $(this).is(':checked');
        console.log("0")
        map.setLayoutProperty('bikes', 'visibility', 'visible');
        map.setLayoutProperty('docks', 'visibility', 'none');
    }
});

