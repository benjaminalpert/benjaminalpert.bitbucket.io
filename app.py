from flask import Flask, render_template, redirect, url_for, request, jsonify
import urllib2
import json
import geopandas as gpd
import pandas as pd

app = Flask(__name__)

def get_citibike():
    url = "https://gbfs.citibikenyc.com/gbfs/es/station_status.json"
    base = gpd.read_file("static/data/base.geojson")
    try:
        req = urllib2.urlopen(url)
        data = json.load(req)
        stats = data['data']['stations']
        curr = pd.DataFrame(stats)[['station_id','num_bikes_available','num_docks_available']]
        curr = gpd.GeoDataFrame(pd.merge(curr, base))
        if os.path.isfile("static/data/citibike.geojson"):
            os.remove("static/data/citibike.geojson")
        curr.to_file("static/data/citibike.geojson", driver="GeoJSON")
    except:
        print "failed"

@app.route('/')
def home():
    return redirect(url_for('login'))


@app.route('/jbvfdainhdsagflkjk6472631bsvdafvubhfaoishrg70945njbol')
def welcome():
    get_citibike()
    return render_template('index.html')

@app.route('/login', methods=['GET','POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['password'] != "handle":
            error = 'Invalid Credentials'
        else:
            return redirect(url_for('welcome'))
    return render_template('login.html', error=error)

if __name__ == '__main__':
    app.run(debug=True)